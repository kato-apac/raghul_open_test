view: invoices {
  sql_table_name: open_prod.invoices ;;
  drill_fields: [reference_invoices_id]

  dimension: reference_invoices_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.reference_invoices_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: additional_charge {
    type: number
    sql: ${TABLE}.additional_charge ;;
  }

  dimension: address_id_billing {
    type: number
    value_format_name: id
    sql: ${TABLE}.address_id_billing ;;
  }

  dimension: address_id_shipping {
    type: number
    value_format_name: id
    sql: ${TABLE}.address_id_shipping ;;
  }

  dimension: adjustment_value {
    type: number
    sql: ${TABLE}.adjustment_value ;;
  }

  dimension: allow_partial_payment {
    type: number
    sql: ${TABLE}.allow_partial_payment ;;
  }

  dimension: batch_id {
    type: string
    sql: ${TABLE}.batch_id ;;
  }

  dimension: bill_amount {
    type: number
    sql: ${TABLE}.bill_amount ;;
  }

  dimension_group: bill_due {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.bill_due_date ;;
  }

  dimension: bill_ref_no {
    type: string
    sql: ${TABLE}.bill_ref_no ;;
  }

  dimension_group: challan {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.challan_date ;;
  }

  dimension: challan_no {
    type: string
    sql: ${TABLE}.challan_no ;;
  }

  dimension: companies_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.companies_id ;;
  }

  dimension: contacts_id {
    type: number
    sql: ${TABLE}.contacts_id ;;
  }

  dimension: converted_amount {
    type: number
    sql: ${TABLE}.converted_amount ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: creation_types_id {
    type: number
    sql: ${TABLE}.creation_types_id ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: current_counter {
    type: number
    sql: ${TABLE}.current_counter ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: discount_amount {
    type: number
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_perc {
    type: number
    sql: ${TABLE}.discount_perc ;;
  }

  dimension: domain {
    type: string
    sql: ${TABLE}.domain ;;
  }

  dimension_group: due_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.due_date ;;
  }

  dimension: email_id {
    type: string
    sql: ${TABLE}.email_id ;;
  }

  dimension: emandate_statuses_id {
    type: number
    sql: ${TABLE}.emandate_statuses_id ;;
  }

  dimension_group: end_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension: estimate_id {
    type: number
    sql: ${TABLE}.estimate_id ;;
  }

  dimension: estimate_invoices_id {
    type: number
    sql: ${TABLE}.estimate_invoices_id ;;
  }

  dimension_group: expiry {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expiry_date ;;
  }

  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }

  dimension: frequencies_id {
    type: number
    sql: ${TABLE}.frequencies_id ;;
  }

  dimension: gst_input_credit {
    type: number
    sql: ${TABLE}.gst_input_credit ;;
  }

  dimension: gst_perc {
    type: number
    sql: ${TABLE}.gst_perc ;;
  }

  dimension: gst_value {
    type: number
    sql: ${TABLE}.gst_value ;;
  }

  dimension: income_categories_id {
    type: number
    sql: ${TABLE}.income_categories_id ;;
  }

  dimension: invoice_note {
    type: string
    sql: ${TABLE}.invoice_note ;;
  }

  dimension_group: invoice_sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.invoice_sent_date ;;
  }

  dimension: invoice_sequence_id {
    type: string
    sql: ${TABLE}.invoice_sequence_id ;;
  }

  dimension: invoice_statuses_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.invoice_statuses_id ;;
  }

  dimension: invoice_types_id {
    type: number
    sql: ${TABLE}.invoice_types_id ;;
  }

  dimension: invoice_user_input_reference_id {
    type: string
    sql: ${TABLE}.invoice_user_input_reference_id ;;
  }

  dimension: invoices_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.invoices_id ;;
  }

  dimension: is_amount_editable {
    type: number
    sql: ${TABLE}.is_amount_editable ;;
  }

  dimension: is_draft {
    type: string
    sql: ${TABLE}.is_draft ;;
  }

  dimension: is_emandate_requested {
    type: number
    sql: ${TABLE}.is_emandate_requested ;;
  }

  dimension: is_enterprise {
    type: number
    sql: ${TABLE}.is_enterprise ;;
  }

  dimension: is_international {
    type: number
    sql: ${TABLE}.is_international ;;
  }

  dimension: is_one_time_payment {
    type: number
    sql: ${TABLE}.is_one_time_payment ;;
  }

  dimension: is_override {
    type: number
    sql: ${TABLE}.is_override ;;
  }

  dimension: is_pg_enabled {
    type: number
    sql: ${TABLE}.is_pg_enabled ;;
  }

  dimension: is_recurring {
    type: number
    sql: ${TABLE}.is_recurring ;;
  }

  dimension: is_service_transaction {
    type: number
    sql: ${TABLE}.is_service_transaction ;;
  }

  dimension: is_tnc_required {
    type: number
    sql: ${TABLE}.is_tnc_required ;;
  }

  dimension: item_ids {
    type: string
    sql: ${TABLE}.item_ids ;;
  }

  dimension: l2_categories_id {
    type: number
    sql: ${TABLE}.l2_categories_id ;;
  }

  dimension: l3_categories_id {
    type: number
    sql: ${TABLE}.l3_categories_id ;;
  }

  dimension_group: last_paid {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_paid_at ;;
  }

  dimension_group: last_send {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_send_date ;;
  }

  dimension: link_expired {
    type: number
    sql: ${TABLE}.link_expired ;;
  }

  dimension: mobile_number {
    type: string
    sql: ${TABLE}.mobile_number ;;
  }

  dimension: net_total {
    type: number
    sql: ${TABLE}.net_total ;;
  }

  dimension: net_total_after_taxes {
    type: number
    sql: ${TABLE}.net_total_after_taxes ;;
  }

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension: order_no {
    type: string
    sql: ${TABLE}.order_no ;;
  }

  dimension: parent_invoices_id {
    type: number
    sql: ${TABLE}.parent_invoices_id ;;
  }

  dimension: payment_link_click_counter {
    type: number
    sql: ${TABLE}.payment_link_click_counter ;;
  }

  dimension: payment_link_unique_id {
    type: string
    sql: ${TABLE}.payment_link_unique_id ;;
  }

  dimension: payment_ref_num {
    type: string
    sql: ${TABLE}.payment_ref_num ;;
  }

  dimension: payment_token {
    type: string
    sql: ${TABLE}.payment_token ;;
  }

  dimension: payments_terms_id {
    type: string
    sql: ${TABLE}.payments_terms_id ;;
  }

  dimension: place_of_supply {
    type: string
    sql: ${TABLE}.place_of_supply ;;
  }

  dimension: qc_balance {
    type: number
    sql: ${TABLE}.qc_balance ;;
  }

  dimension: receive_id {
    type: string
    sql: ${TABLE}.receive_id ;;
  }

  dimension: recepient_name {
    type: string
    sql: ${TABLE}.recepient_name ;;
  }

  dimension: recurrence_completed {
    type: number
    sql: ${TABLE}.recurrence_completed ;;
  }

  dimension: recurrence_pending {
    type: number
    sql: ${TABLE}.recurrence_pending ;;
  }

  dimension: recurring_invoices_id {
    type: number
    sql: ${TABLE}.recurring_invoices_id ;;
  }

  dimension: reference_number {
    type: string
    sql: ${TABLE}.reference_number ;;
  }

  dimension: remarks {
    type: string
    sql: ${TABLE}.remarks ;;
  }

  dimension: repeat_for {
    type: number
    sql: ${TABLE}.repeat_for ;;
  }

  dimension: round_off {
    type: number
    sql: ${TABLE}.round_off ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: tds_amount {
    type: number
    sql: ${TABLE}.tds_amount ;;
  }

  dimension: tds_l3_categories_id {
    type: number
    sql: ${TABLE}.tds_l3_categories_id ;;
  }

  dimension: tds_perc {
    type: number
    sql: ${TABLE}.tds_perc ;;
  }

  dimension: total_amount_due {
    type: number
    sql: ${TABLE}.total_amount_due ;;
  }

  dimension: total_counter {
    type: number
    sql: ${TABLE}.total_counter ;;
  }

  dimension: transaction_client_types_id {
    type: number
    sql: ${TABLE}.transaction_client_types_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_agent {
    type: string
    sql: ${TABLE}.user_agent ;;
  }

  dimension: users_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      reference_invoices_id,
      recepient_name,
      companies.brand_name,
      companies.company_name,
      companies.companies_id,
      invoice_statuses.invoice_statuses_id,
      invoices.recepient_name,
      invoices.reference_invoices_id,
      users.last_name,
      users.first_name,
      users.username,
      users.users_id,
      invoices.count
    ]
  }
}
