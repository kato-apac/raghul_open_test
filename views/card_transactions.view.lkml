view: card_transactions {
  sql_table_name: open_prod.card_transactions ;;
  drill_fields: [card_transactions_id]

  dimension: card_transactions_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.card_transactions_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: atm_fees {
    type: number
    sql: ${TABLE}.atm_fees ;;
  }

  dimension: balance {
    type: number
    sql: ${TABLE}.balance ;;
  }

  dimension: card_category_id {
    type: number
    sql: ${TABLE}.card_category_id ;;
  }

  dimension: card_details_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.card_details_id ;;
  }

  dimension_group: card_transaction {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.card_transaction_date ;;
  }

  dimension: card_transaction_ref_no {
    type: string
    sql: ${TABLE}.card_transaction_ref_no ;;
  }

  dimension: card_transaction_statuses_id {
    type: number
    sql: ${TABLE}.card_transaction_statuses_id ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: m2p_txn_currency {
    type: string
    sql: ${TABLE}.m2p_txn_currency ;;
  }

  dimension: masked_card_number {
    type: string
    sql: ${TABLE}.masked_card_number ;;
  }

  dimension: mcc_id {
    type: number
    sql: ${TABLE}.mcc_id ;;
  }

  dimension: merchant_id {
    type: string
    sql: ${TABLE}.merchant_id ;;
  }

  dimension: merchant_location {
    type: string
    sql: ${TABLE}.merchant_location ;;
  }

  dimension: merchant_name {
    type: string
    sql: ${TABLE}.merchant_name ;;
  }

  dimension: remarks {
    type: string
    sql: ${TABLE}.remarks ;;
  }

  dimension: retrieval_ref_no {
    type: string
    sql: ${TABLE}.retrieval_ref_no ;;
  }

  dimension: source_type_name {
    type: string
    sql: ${TABLE}.source_type_name ;;
  }

  dimension: source_type_transaction_id {
    type: number
    sql: ${TABLE}.source_type_transaction_id ;;
  }

  dimension: team_member_details_id {
    type: number
    sql: ${TABLE}.team_member_details_id ;;
  }

  dimension: terminal_id {
    type: string
    sql: ${TABLE}.terminal_id ;;
  }

  dimension: transaction_mode {
    type: string
    sql: ${TABLE}.transaction_mode ;;
  }

  dimension: txn_currency {
    type: string
    sql: ${TABLE}.txn_currency ;;
  }

  dimension: txn_id {
    type: string
    sql: ${TABLE}.txn_id ;;
  }

  dimension: txn_origin {
    type: string
    sql: ${TABLE}.txn_origin ;;
  }

  dimension: txn_status {
    type: string
    sql: ${TABLE}.txn_status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [card_transactions_id, source_type_name, merchant_name, card_details.card_details_id]
  }
}
