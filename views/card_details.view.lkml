view: card_details {
  sql_table_name: open_prod.card_details ;;
  drill_fields: [card_details_id]

  dimension: card_details_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.card_details_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: activation_period {
    type: string
    sql: ${TABLE}.activation_period ;;
  }

  dimension: activation_status_message {
    type: string
    sql: ${TABLE}.activation_status_message ;;
  }

  dimension: balance_limit {
    type: number
    sql: ${TABLE}.balance_limit ;;
  }

  dimension: caf_s3_url {
    type: string
    sql: ${TABLE}.caf_s3_url ;;
  }

  dimension_group: card_activation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.card_activation_date ;;
  }

  dimension: card_activation_statuses_id {
    type: number
    sql: ${TABLE}.card_activation_statuses_id ;;
  }

  dimension: card_category_id {
    type: number
    sql: ${TABLE}.card_category_id ;;
  }

  dimension: card_labels_id {
    type: number
    sql: ${TABLE}.card_labels_id ;;
  }

  dimension: card_load_amount {
    type: number
    sql: ${TABLE}.card_load_amount ;;
  }

  dimension: card_load_limit {
    type: number
    sql: ${TABLE}.card_load_limit ;;
  }

  dimension: card_provider {
    type: string
    sql: ${TABLE}.card_provider ;;
  }

  dimension: card_ref_id {
    type: string
    sql: ${TABLE}.card_ref_id ;;
  }

  dimension_group: card_status_last_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.card_status_last_updated_at ;;
  }

  dimension: card_types_id {
    type: number
    sql: ${TABLE}.card_types_id ;;
  }

  dimension: cif_no {
    type: string
    sql: ${TABLE}.cif_no ;;
  }

  dimension: companies_card_statuses_id {
    type: number
    sql: ${TABLE}.companies_card_statuses_id ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current_balance {
    type: number
    sql: ${TABLE}.current_balance ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension_group: expiration {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.expiration_date ;;
  }

  dimension: is_kyc_document_verified {
    type: number
    sql: ${TABLE}.is_kyc_document_verified ;;
  }

  dimension: is_kyc_documents_submitted {
    type: number
    sql: ${TABLE}.is_kyc_documents_submitted ;;
  }

  dimension: masked_card_number {
    type: string
    sql: ${TABLE}.masked_card_number ;;
  }

  dimension: monthly_limit {
    type: number
    sql: ${TABLE}.monthly_limit ;;
  }

  dimension: name_on_card {
    type: string
    sql: ${TABLE}.name_on_card ;;
  }

  dimension: open_card_statuses_id {
    type: number
    sql: ${TABLE}.open_card_statuses_id ;;
  }

  dimension: open_card_type {
    type: string
    sql: ${TABLE}.open_card_type ;;
  }

  dimension: owner_card {
    type: number
    sql: ${TABLE}.owner_card ;;
  }

  dimension: set_pin_status {
    type: number
    sql: ${TABLE}.set_pin_status ;;
  }

  dimension: share_code {
    type: string
    sql: ${TABLE}.share_code ;;
  }

  dimension: source_id {
    type: number
    sql: ${TABLE}.source_id ;;
  }

  dimension: source_of_registration {
    type: string
    sql: ${TABLE}.source_of_registration ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }

  dimension: team_member_details_id {
    type: number
    sql: ${TABLE}.team_member_details_id ;;
  }

  dimension_group: updated_at {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: used_limit {
    type: number
    sql: ${TABLE}.used_limit ;;
  }

  dimension: users_card_statuses_id {
    type: number
    sql: ${TABLE}.users_card_statuses_id ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [card_details_id, card_transactions.count]
  }
}
