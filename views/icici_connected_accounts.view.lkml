view: icici_connected_accounts {
  sql_table_name: open_prod.icici_connected_accounts ;;
  drill_fields: [icici_connected_accounts_id]

  dimension: icici_connected_accounts_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.icici_connected_accounts_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: aggr_id {
    type: string
    sql: ${TABLE}.aggr_id ;;
  }

  dimension: aggr_name {
    type: string
    sql: ${TABLE}.aggr_name ;;
  }

  dimension: alias_id {
    type: string
    sql: ${TABLE}.alias_id ;;
  }

  dimension: bank_account_number {
    type: string
    sql: ${TABLE}.bank_account_number ;;
  }

  dimension: bank_connection_statuses_id {
    type: number
    sql: ${TABLE}.bank_connection_statuses_id ;;
  }

  dimension: cid {
    type: string
    sql: ${TABLE}.cid ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension: is_callback_successfull {
    type: number
    sql: ${TABLE}.is_callback_successfull ;;
  }

  dimension: uid {
    type: string
    sql: ${TABLE}.uid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: urn {
    type: string
    sql: ${TABLE}.urn ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [icici_connected_accounts_id, aggr_name]
  }
}
