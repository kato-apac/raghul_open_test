view: external_fund_transfers {
  sql_table_name: open_prod.external_fund_transfers ;;
  drill_fields: [external_fund_transfers_id]

  dimension: external_fund_transfers_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.external_fund_transfers_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: api_error_message {
    type: string
    sql: ${TABLE}.api_error_message ;;
  }

  dimension: bank_transaction_status_id {
    type: number
    sql: ${TABLE}.bank_transaction_status_id ;;
  }

  dimension: batch_id {
    type: string
    sql: ${TABLE}.batch_id ;;
  }

  dimension: beneficiary_account_number {
    type: string
    sql: ${TABLE}.beneficiary_account_number ;;
  }

  dimension: beneficiary_ifsc_code {
    type: string
    sql: ${TABLE}.beneficiary_ifsc_code ;;
  }

  dimension: client_ip {
    type: string
    sql: ${TABLE}.client_ip ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension: contacts_id {
    type: number
    sql: ${TABLE}.contacts_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: creation_types_id {
    type: number
    sql: ${TABLE}.creation_types_id ;;
  }

  dimension: debit_account_number {
    type: string
    sql: ${TABLE}.debit_account_number ;;
  }

  dimension: email_id {
    type: string
    sql: ${TABLE}.email_id ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension: expense_categories_id {
    type: number
    sql: ${TABLE}.expense_categories_id ;;
  }

  dimension: frequencies_id {
    type: number
    sql: ${TABLE}.frequencies_id ;;
  }

  dimension: internal_transaction_ref_id {
    type: string
    sql: ${TABLE}.internal_transaction_ref_id ;;
  }

  dimension: is_cancelled {
    type: number
    sql: ${TABLE}.is_cancelled ;;
  }

  dimension: is_contact {
    type: number
    sql: ${TABLE}.is_contact ;;
  }

  dimension: l2_categories_id {
    type: number
    sql: ${TABLE}.l2_categories_id ;;
  }

  dimension: l3_categories_id {
    type: number
    sql: ${TABLE}.l3_categories_id ;;
  }

  dimension: latitude {
    type: string
    sql: ${TABLE}.latitude ;;
  }

  dimension: link_types_id {
    type: number
    sql: ${TABLE}.link_types_id ;;
  }

  dimension: longitude {
    type: string
    sql: ${TABLE}.longitude ;;
  }

  dimension: merchant_ref_id {
    type: string
    sql: ${TABLE}.merchant_ref_id ;;
  }

  dimension: mobile_number {
    type: string
    sql: ${TABLE}.mobile_number ;;
  }

  dimension_group: next_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.next_payment_date ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }

  dimension: open_bank_accounts_id {
    type: number
    sql: ${TABLE}.open_bank_accounts_id ;;
  }

  dimension: partner_banks_id {
    type: number
    sql: ${TABLE}.partner_banks_id ;;
  }

  dimension: payee_vpa {
    type: string
    sql: ${TABLE}.payee_vpa ;;
  }

  dimension: payment_count {
    type: number
    sql: ${TABLE}.payment_count ;;
  }

  dimension: payment_schedules_id {
    type: number
    sql: ${TABLE}.payment_schedules_id ;;
  }

  dimension: purpose {
    type: string
    sql: ${TABLE}.purpose ;;
  }

  dimension: recepient_name {
    type: string
    sql: ${TABLE}.recepient_name ;;
  }

  dimension: repeat_for {
    type: number
    sql: ${TABLE}.repeat_for ;;
  }

  dimension: source_id {
    type: number
    sql: ${TABLE}.source_id ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: transaction_client_types_id {
    type: number
    sql: ${TABLE}.transaction_client_types_id ;;
  }

  dimension: transaction_types_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.transaction_types_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  dimension: withdraw_bank_accounts_id {
    type: number
    sql: ${TABLE}.withdraw_bank_accounts_id ;;
  }

  measure: count {
    type: count
    drill_fields: [external_fund_transfers_id, recepient_name, transaction_types.name, transaction_types.transaction_types_id]
  }

  measure: sum {
    type: sum
    sql: ${amount} ;;

  }
}
