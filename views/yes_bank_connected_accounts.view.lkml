view: yes_bank_connected_accounts {
  sql_table_name: open_prod.yes_bank_connected_accounts ;;
  drill_fields: [yes_bank_connected_accounts_id]

  dimension: yes_bank_connected_accounts_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.yes_bank_connected_accounts_id ;;
  }

  dimension: account_number {
    type: string
    sql: ${TABLE}.account_number ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: app_id {
    type: string
    sql: ${TABLE}.app_id ;;
  }

  dimension: bank_connection_statuses_id {
    type: number
    sql: ${TABLE}.bank_connection_statuses_id ;;
  }

  dimension: br_file_id {
    type: string
    sql: ${TABLE}.br_file_id ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: customer_id {
    type: string
    sql: ${TABLE}.customer_id ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension: mc_file_id {
    type: string
    sql: ${TABLE}.mc_file_id ;;
  }

  dimension: setup_file_id {
    type: string
    sql: ${TABLE}.setup_file_id ;;
  }

  dimension: setup_file_url {
    type: string
    sql: ${TABLE}.setup_file_url ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [yes_bank_connected_accounts_id]
  }
}
