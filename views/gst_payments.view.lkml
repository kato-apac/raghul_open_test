view: gst_payments {
  sql_table_name: open_prod.gst_payments ;;
  drill_fields: [gst_payments_id]

  dimension: gst_payments_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.gst_payments_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: beneficiary_account_number {
    type: string
    sql: ${TABLE}.beneficiary_account_number ;;
  }

  dimension: beneficiary_ifsc_code {
    type: string
    sql: ${TABLE}.beneficiary_ifsc_code ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: debit_account_number {
    type: string
    sql: ${TABLE}.debit_account_number ;;
  }

  dimension: internal_transaction_ref_id {
    type: string
    sql: ${TABLE}.internal_transaction_ref_id ;;
  }

  dimension: is_success {
    type: number
    sql: ${TABLE}.is_success ;;
  }

  dimension: l2_categories_id {
    type: number
    sql: ${TABLE}.l2_categories_id ;;
  }

  dimension: l3_categories_id {
    type: number
    sql: ${TABLE}.l3_categories_id ;;
  }

  dimension: link_types_id {
    type: number
    sql: ${TABLE}.link_types_id ;;
  }

  dimension: open_bank_accounts_id {
    type: number
    sql: ${TABLE}.open_bank_accounts_id ;;
  }

  dimension: partner_banks_id {
    type: number
    sql: ${TABLE}.partner_banks_id ;;
  }

  dimension: payment_schedules_id {
    type: number
    sql: ${TABLE}.payment_schedules_id ;;
  }

  dimension: recepient_name {
    type: string
    sql: ${TABLE}.recepient_name ;;
  }

  dimension: transaction_client_types_id {
    type: number
    sql: ${TABLE}.transaction_client_types_id ;;
  }

  dimension_group: transaction {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transaction_date ;;
  }

  dimension: transaction_types_id {
    type: number
    sql: ${TABLE}.transaction_types_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [gst_payments_id, recepient_name]
  }
}
