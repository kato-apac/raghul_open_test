view: onboarding_statuses {
  sql_table_name: open_prod.onboarding_statuses ;;
  drill_fields: [onboarding_statuses_id]

  dimension: onboarding_statuses_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.onboarding_statuses_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: is_active {
    type: number
    sql: ${TABLE}.is_active ;;
  }

  dimension: status_name {
    type: string
    sql: ${TABLE}.status_name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [onboarding_statuses_id, status_name, companies.count]
  }
}
