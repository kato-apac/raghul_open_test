view: team_invites {
  sql_table_name: open_prod.team_invites ;;
  drill_fields: [team_invites_id]

  dimension: team_invites_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.team_invites_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension: emp_code {
    type: string
    sql: ${TABLE}.emp_code ;;
  }

  dimension: hash {
    type: string
    sql: ${TABLE}.hash ;;
  }

  dimension_group: invite_accepted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.invite_accepted_on ;;
  }

  dimension_group: invite_sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.invite_sent_on ;;
  }

  dimension: is_invite_accepted {
    type: number
    sql: ${TABLE}.is_invite_accepted ;;
  }

  dimension: is_invite_link_disabled {
    type: number
    sql: ${TABLE}.is_invite_link_disabled ;;
  }

  dimension: joining_statuses_id {
    type: number
    sql: ${TABLE}.joining_statuses_id ;;
  }

  dimension: request_data {
    type: string
    sql: ${TABLE}.request_data ;;
  }

  dimension: sent_to_email {
    type: string
    sql: ${TABLE}.sent_to_email ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  measure: count {
    type: count
    drill_fields: [team_invites_id]
  }
}
