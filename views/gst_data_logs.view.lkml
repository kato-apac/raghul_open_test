view: gst_data_logs {
  sql_table_name: open_prod.gst_data_logs ;;
  drill_fields: [gst_data_logs_id]

  dimension: gst_data_logs_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.gst_data_logs_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: action {
    type: string
    sql: ${TABLE}.action ;;
  }

  dimension: companies_id {
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: data {
    type: string
    sql: ${TABLE}.data ;;
  }

  dimension: data_type {
    type: string
    sql: ${TABLE}.data_type ;;
  }

  dimension_group: deleted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.deleted_at ;;
  }

  dimension: eligible_itc {
    type: number
    sql: ${TABLE}.eligible_itc ;;
  }

  dimension: file_path {
    type: string
    sql: ${TABLE}.file_path ;;
  }

  dimension: financial_year {
    type: string
    sql: ${TABLE}.financial_year ;;
  }

  dimension: gstin {
    type: string
    sql: ${TABLE}.gstin ;;
  }

  dimension: liability {
    type: number
    sql: ${TABLE}.liability ;;
  }

  dimension_group: period {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.period ;;
  }

  dimension: sales_turn_over {
    type: number
    sql: ${TABLE}.sales_turn_over ;;
  }

  dimension: tax_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.tax_paid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  dimension: users_id {
    type: number
    sql: ${TABLE}.users_id ;;
  }

  dimension: visible_to {
    type: number
    sql: ${TABLE}.visible_to ;;
  }

  measure: count {
    type: count
    drill_fields: [gst_data_logs_id, username]
  }
}
