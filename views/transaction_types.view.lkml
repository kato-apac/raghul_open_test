view: transaction_types {
  sql_table_name: open_prod.transaction_types ;;
  drill_fields: [transaction_types_id]

  dimension: transaction_types_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.transaction_types_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [transaction_types_id, name, external_fund_transfers.count]
  }
}
