view: companies {
  sql_table_name: open_prod.companies ;;
  drill_fields: [companies_id]

  dimension: companies_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.companies_id ;;
  }

  dimension: accounts_id {
    type: number
    sql: ${TABLE}.accounts_id ;;
  }

  dimension: api_version {
    type: string
    sql: ${TABLE}.api_version ;;
  }

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: business_account_types_id {
    type: number
    sql: ${TABLE}.business_account_types_id ;;
  }

  dimension: companies_va_number {
    type: string
    sql: ${TABLE}.companies_va_number ;;
  }

  dimension: companies_va_number_temp {
    type: string
    sql: ${TABLE}.companies_va_number_temp ;;
  }

  dimension: company_cin {
    type: string
    sql: ${TABLE}.company_cin ;;
  }

  dimension: company_cst {
    type: string
    sql: ${TABLE}.company_cst ;;
  }

  dimension: company_gstin {
    type: string
    sql: ${TABLE}.company_gstin ;;
  }

  dimension: company_id {
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension: company_mcc_types_id {
    type: number
    sql: ${TABLE}.company_mcc_types_id ;;
  }

  dimension: company_name {
    type: string
    sql: ${TABLE}.company_name ;;
  }

  dimension: company_pan {
    type: string
    sql: ${TABLE}.company_pan ;;
  }

  dimension: company_phone {
    type: string
    sql: ${TABLE}.company_phone ;;
  }

  dimension: company_tan {
    type: string
    sql: ${TABLE}.company_tan ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current_account_applied {
    type: number
    sql: ${TABLE}.current_account_applied ;;
  }

  dimension: fund_transfer_partner_banks_id {
    type: number
    sql: ${TABLE}.fund_transfer_partner_banks_id ;;
  }

  dimension: has_icici_connected {
    type: number
    sql: ${TABLE}.has_icici_connected ;;
  }

  dimension: is_auto_verified {
    type: number
    sql: ${TABLE}.is_auto_verified ;;
  }

  dimension: is_ekyc_verified {
    type: number
    sql: ${TABLE}.is_ekyc_verified ;;
  }

  dimension: is_joint_account {
    type: number
    sql: ${TABLE}.is_joint_account ;;
  }

  dimension: is_offline_merchant {
    type: number
    sql: ${TABLE}.is_offline_merchant ;;
  }

  dimension: is_short_kyc_user {
    type: number
    sql: ${TABLE}.is_short_kyc_user ;;
  }

  dimension: mob_business_types_id {
    type: number
    sql: ${TABLE}.mob_business_types_id ;;
  }

  dimension: mob_statuses_id {
    type: number
    sql: ${TABLE}.mob_statuses_id ;;
  }

  dimension_group: onboarding_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.onboarding_completed_on ;;
  }

  dimension: onboarding_statuses_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.onboarding_statuses_id ;;
  }

  dimension_group: openbook_kyc_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.openbook_kyc_completed_on ;;
  }

  dimension: partner_banks_id {
    type: string
    sql: ${TABLE}.partner_banks_id ;;
  }

  dimension: partner_banks_id_temp {
    type: string
    sql: ${TABLE}.partner_banks_id_temp ;;
  }

  dimension: referral {
    type: string
    sql: ${TABLE}.referral ;;
  }

  dimension_group: seller_tagged {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.seller_tagged_on ;;
  }

  dimension: seller_types_id {
    type: number
    sql: ${TABLE}.seller_types_id ;;
  }

  dimension: service_tax_number {
    type: string
    sql: ${TABLE}.service_tax_number ;;
  }

  dimension: smid {
    type: string
    sql: ${TABLE}.smid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: users_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.users_id ;;
  }

  dimension: vat_tin_number {
    type: string
    sql: ${TABLE}.vat_tin_number ;;
  }

  dimension: vpa_partner_banks_id {
    type: number
    sql: ${TABLE}.vpa_partner_banks_id ;;
  }

  dimension: website {
    type: string
    sql: ${TABLE}.website ;;
  }

  dimension: website_description {
    type: string
    sql: ${TABLE}.website_description ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      companies_id,
      brand_name,
      company_name,
      onboarding_statuses.status_name,
      onboarding_statuses.onboarding_statuses_id,
      users.last_name,
      users.first_name,
      users.username,
      users.users_id,
      invoices.count
    ]
  }
}
