view: mob_business_types {
  sql_table_name: open_prod.mob_business_types ;;
  drill_fields: [mob_business_types_id]

  dimension: mob_business_types_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.mob_business_types_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: payswiff_filing_types {
    type: string
    sql: ${TABLE}.payswiff_filing_types ;;
  }

  dimension: slug {
    type: string
    sql: ${TABLE}.slug ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: yes_bank_vpa_business_types_id {
    type: number
    sql: ${TABLE}.yes_bank_vpa_business_types_id ;;
  }

  measure: count {
    type: count
    drill_fields: [mob_business_types_id, name]
  }
}
