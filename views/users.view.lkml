view: users {
  sql_table_name: open_prod.users ;;
  drill_fields: [users_id]

  dimension: users_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.users_id ;;
  }

  dimension: api_version {
    type: string
    sql: ${TABLE}.api_version ;;
  }

  dimension: ban_reason {
    type: string
    sql: ${TABLE}.ban_reason ;;
  }

  dimension: client_ip {
    type: string
    sql: ${TABLE}.client_ip ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: disabled_reason {
    type: string
    sql: ${TABLE}.disabled_reason ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: email_verification_status {
    type: number
    sql: ${TABLE}.email_verification_status ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: has_email_verified {
    type: number
    sql: ${TABLE}.has_email_verified ;;
  }

  dimension: has_mobile_number_verified {
    type: number
    sql: ${TABLE}.has_mobile_number_verified ;;
  }

  dimension: is_banned {
    type: number
    sql: ${TABLE}.is_banned ;;
  }

  dimension: is_data_migrated {
    type: number
    sql: ${TABLE}.is_data_migrated ;;
  }

  dimension: is_deleted {
    type: number
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: is_disabled {
    type: number
    sql: ${TABLE}.is_disabled ;;
  }

  dimension: is_journal_entry_created {
    type: number
    sql: ${TABLE}.is_journal_entry_created ;;
  }

  dimension: is_migration_notified {
    type: number
    sql: ${TABLE}.is_migration_notified ;;
  }

  dimension: is_user_used_app_migrated {
    type: number
    sql: ${TABLE}.is_user_used_app_migrated ;;
  }

  dimension_group: last_login {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_login ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: my_referral_code {
    type: string
    sql: ${TABLE}.my_referral_code ;;
  }

  dimension: password {
    type: string
    sql: ${TABLE}.password ;;
  }

  dimension: permissions {
    type: string
    sql: ${TABLE}.permissions ;;
  }

  dimension: skip_onboarding {
    type: number
    sql: ${TABLE}.skip_onboarding ;;
  }

  dimension: social_login_providers_id {
    type: number
    sql: ${TABLE}.social_login_providers_id ;;
  }

  dimension: source_of_registration {
    type: string
    sql: ${TABLE}.source_of_registration ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_role_in_company {
    type: string
    sql: ${TABLE}.user_role_in_company ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      users_id,
      last_name,
      first_name,
      username,
      companies.count,
      invoices.count
    ]
  }
}
