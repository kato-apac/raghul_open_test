- dashboard: open_money_payout_metrics
  title: Open Money Payout metrics
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  refresh: 1 day
  elements:
  - title: MOM Payout Count and volume (open_va)
    name: MOM Payout Count and volume (open_va)
    model: raghul_test_model
    explore: external_fund_transfers
    type: looker_line
    fields: [external_fund_transfers.created_month, external_fund_transfers.count,
      external_fund_transfers.sum]
    fill_fields: [external_fund_transfers.created_month]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.created_month: 6 months
      external_fund_transfers.link_types_id: '1'
    sorts: [external_fund_transfers.created_month desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}\
          \ / 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: sum_in_crore, id: sum_in_crore,
            name: Sum in crore}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, type: linear}]
    hidden_series: []
    series_types:
      sum_in_crore: column
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#E52592"
      sum_in_billion_rupee: "#079c98"
      sum_in_billion: "#079c98"
      sum_in_crore: "#a020f0"
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum]
    listen: {}
    row: 0
    col: 0
    width: 12
    height: 5
  - title: Payouts modes MOM count and volume (open_va)
    name: Payouts modes MOM count and volume (open_va)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_grid
    fields: [transaction_types.name, external_fund_transfers.count, external_fund_transfers.sum,
      external_fund_transfers.created_month]
    pivots: [external_fund_transfers.created_month]
    fill_fields: [external_fund_transfers.created_month]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.created_month: 5 months
      transaction_types.name: "-NULL"
      external_fund_transfers.link_types_id: '1'
      external_fund_transfers.partner_banks_id: ''
    sorts: [external_fund_transfers.count 0, transaction_types.name, external_fund_transfers.created_month]
    limit: 500
    total: true
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}/\
          \ 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number},
      {category: table_calculation, label: "% Contribution", value_format: !!null '',
        value_format_name: percent_1, calculation_type: percent_of_column_sum, table_calculation: contribution,
        args: [external_fund_transfers.count], _kind_hint: measure, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: true
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    series_cell_visualizations:
      external_fund_transfers.count:
        is_active: false
    series_text_format:
      sum_in_crore:
        italic: true
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          custom: {id: effb8932-8fd8-8909-dc9b-4179081ff2b6, label: Custom, type: continuous,
            stops: [{color: "#a020f0", offset: 0}, {color: "#E52592", offset: 100}]},
          options: {steps: 5}}, bold: false, italic: false, strikethrough: false,
        fields: [external_fund_transfers.count]}]
    truncate_column_names: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: external_fund_transfers.sum,
            id: external_fund_transfers.sum, name: Sum}], showLabels: false, showValues: false,
        unpinAxis: false, tickDensity: default, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    hidden_series: []
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#E8710A"
      sum_in_billion_rupee: "#079c98"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    defaults_version: 1
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum, contribution]
    listen: {}
    row: 5
    col: 0
    width: 24
    height: 5
  - title: MOM Payout Count and volume (ICICI connected)
    name: MOM Payout Count and volume (ICICI connected)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_line
    fields: [external_fund_transfers.created_month, external_fund_transfers.count,
      external_fund_transfers.sum]
    fill_fields: [external_fund_transfers.created_month]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.created_month: 6 months
      external_fund_transfers.link_types_id: '3'
      external_fund_transfers.partner_banks_id: '1'
    sorts: [external_fund_transfers.created_month desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}\
          \ / 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: sum_in_crore, id: sum_in_crore,
            name: Sum in crore}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, type: linear}]
    hidden_series: []
    series_types:
      sum_in_crore: column
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#B02A30"
      sum_in_billion_rupee: "#079c98"
      sum_in_billion: "#079c98"
      sum_in_crore: "#F99D27"
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum]
    listen: {}
    row: 10
    col: 0
    width: 12
    height: 5
  - title: MOM Payout Count and volume (Yes Bank)
    name: MOM Payout Count and volume (Yes Bank)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_line
    fields: [external_fund_transfers.created_month, external_fund_transfers.count,
      external_fund_transfers.sum]
    fill_fields: [external_fund_transfers.created_month]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.created_month: 6 months
      users.source_of_registration: ''
      external_fund_transfers.link_types_id: '3'
      external_fund_transfers.partner_banks_id: '3'
    sorts: [external_fund_transfers.created_month desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}\
          \ / 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: sum_in_crore, id: sum_in_crore,
            name: Sum in crore}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, type: linear}]
    hidden_series: []
    series_types:
      sum_in_crore: column
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#C4261D"
      sum_in_billion_rupee: "#079c98"
      sum_in_billion: "#079c98"
      sum_in_crore: "#00518F"
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum]
    listen: {}
    row: 20
    col: 0
    width: 12
    height: 5
  - title: MOM Payout Count and volume mode wise (ICICI connected)
    name: MOM Payout Count and volume mode wise (ICICI connected)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_grid
    fields: [transaction_types.name, external_fund_transfers.count, external_fund_transfers.sum,
      external_fund_transfers.created_month]
    pivots: [external_fund_transfers.created_month]
    fill_fields: [external_fund_transfers.created_month]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.created_month: 5 months
      transaction_types.name: "-NULL"
      external_fund_transfers.link_types_id: '3'
      external_fund_transfers.partner_banks_id: '1'
    sorts: [external_fund_transfers.count 0, transaction_types.name, external_fund_transfers.created_month]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}/\
          \ 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number},
      {category: table_calculation, label: "% Contribution", value_format: !!null '',
        value_format_name: percent_1, calculation_type: percent_of_column_sum, table_calculation: contribution,
        args: [external_fund_transfers.count], _kind_hint: measure, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: true
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    series_cell_visualizations:
      external_fund_transfers.count:
        is_active: false
    series_text_format:
      sum_in_crore:
        italic: true
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: b8e44ce6-d0e6-4bd4-b72c-ab0f595726a6, options: {steps: 7, stepped: false,
            reverse: false, mirror: false}}, bold: false, italic: false, strikethrough: false,
        fields: []}, {type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          custom: {id: 19c52d42-45e0-a0b4-374f-a2d623f28ea2, label: Custom, type: continuous,
            stops: [{color: "#F99D27", offset: 0}, {color: "#B02A30", offset: 100}]},
          options: {steps: 5}}, bold: false, italic: false, strikethrough: false,
        fields: [external_fund_transfers.count]}]
    truncate_column_names: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: external_fund_transfers.sum,
            id: external_fund_transfers.sum, name: Sum}], showLabels: false, showValues: false,
        unpinAxis: false, tickDensity: default, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    hidden_series: []
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#E8710A"
      sum_in_billion_rupee: "#079c98"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    defaults_version: 1
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum, contribution]
    listen: {}
    row: 15
    col: 0
    width: 24
    height: 5
  - title: MOM Payout Count and volume mode wise (Yes Bank)
    name: MOM Payout Count and volume mode wise (Yes Bank)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_grid
    fields: [transaction_types.name, external_fund_transfers.count, external_fund_transfers.sum,
      external_fund_transfers.created_month]
    pivots: [external_fund_transfers.created_month]
    fill_fields: [external_fund_transfers.created_month]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.created_month: 5 months
      users.source_of_registration: ''
      transaction_types.name: "-NULL"
      external_fund_transfers.link_types_id: '3'
      external_fund_transfers.partner_banks_id: '3'
    sorts: [external_fund_transfers.count 0, transaction_types.name, external_fund_transfers.created_month]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}/\
          \ 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number},
      {category: table_calculation, label: "% Contribution", value_format: !!null '',
        value_format_name: percent_1, calculation_type: percent_of_column_sum, table_calculation: contribution,
        args: [external_fund_transfers.count], _kind_hint: measure, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: true
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    series_cell_visualizations:
      external_fund_transfers.count:
        is_active: false
    series_text_format:
      sum_in_crore:
        italic: true
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: b8e44ce6-d0e6-4bd4-b72c-ab0f595726a6, options: {steps: 7, stepped: false,
            reverse: false, mirror: false}}, bold: false, italic: false, strikethrough: false,
        fields: []}, {type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          custom: {id: 41c0d037-ff25-fd41-7463-1595fc9bf0ef, label: Custom, type: continuous,
            stops: [{color: "#00518F", offset: 0}, {color: "#C4261D", offset: 100}]},
          options: {steps: 5}}, bold: false, italic: false, strikethrough: false,
        fields: [external_fund_transfers.count]}]
    truncate_column_names: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: external_fund_transfers.sum,
            id: external_fund_transfers.sum, name: Sum}], showLabels: false, showValues: false,
        unpinAxis: false, tickDensity: default, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    hidden_series: []
    legend_position: center
    series_types: {}
    point_style: none
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#E8710A"
      sum_in_billion_rupee: "#079c98"
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    defaults_version: 1
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum, contribution]
    listen: {}
    row: 25
    col: 0
    width: 24
    height: 4
  - title: DOD Payout Count and volume (open_va)
    name: DOD Payout Count and volume (open_va)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_line
    fields: [external_fund_transfers.count, external_fund_transfers.sum, external_fund_transfers.created_date]
    fill_fields: [external_fund_transfers.created_date]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      external_fund_transfers.link_types_id: '1'
      external_fund_transfers.created_date: 7 days
    sorts: [external_fund_transfers.created_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}\
          \ / 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: sum_in_crore, id: sum_in_crore,
            name: Sum in crore}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, type: linear}]
    hidden_series: []
    series_types:
      sum_in_crore: column
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#E52592"
      sum_in_billion_rupee: "#079c98"
      sum_in_billion: "#079c98"
      sum_in_crore: "#a020f0"
    label_rotation:
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum]
    listen: {}
    row: 0
    col: 12
    width: 12
    height: 5
  - title: DOD Payout Count and volume (ICICI connected)
    name: DOD Payout Count and volume (ICICI connected)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_line
    fields: [external_fund_transfers.count, external_fund_transfers.sum, external_fund_transfers.created_date]
    fill_fields: [external_fund_transfers.created_date]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      users.source_of_registration: ''
      external_fund_transfers.link_types_id: '3'
      external_fund_transfers.partner_banks_id: '1'
      external_fund_transfers.created_date: 7 days
    sorts: [external_fund_transfers.created_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}\
          \ / 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: sum_in_crore, id: sum_in_crore,
            name: Sum in crore}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, type: linear}]
    hidden_series: []
    series_types:
      sum_in_crore: column
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#B02A30"
      sum_in_billion_rupee: "#079c98"
      sum_in_billion: "#079c98"
      sum_in_crore: "#F99D27"
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum]
    listen: {}
    row: 10
    col: 12
    width: 12
    height: 5
  - title: DOD Payout Count and volume (Yes Bank)
    name: DOD Payout Count and volume (Yes Bank)
    model: openfinancialtech
    explore: external_fund_transfers
    type: looker_line
    fields: [external_fund_transfers.count, external_fund_transfers.sum, external_fund_transfers.created_date]
    fill_fields: [external_fund_transfers.created_date]
    filters:
      external_fund_transfers.bank_transaction_status_id: '4,15,11,36,103,26,32,33'
      users.source_of_registration: ''
      external_fund_transfers.link_types_id: '3'
      external_fund_transfers.partner_banks_id: '3'
      external_fund_transfers.created_date: 7 days
    sorts: [external_fund_transfers.created_date desc]
    limit: 500
    dynamic_fields: [{category: table_calculation, expression: "${external_fund_transfers.sum}\
          \ / 10000000", label: Sum in crore, value_format: !!null '', value_format_name: decimal_2,
        _kind_hint: measure, table_calculation: sum_in_crore, _type_hint: number}]
    filter_expression: |-
      contains(${users.source_of_registration} ,"app.open.money") OR
      contains(${users.source_of_registration} ,"m.open.money") OR
      contains(${version_migrations.migrated_to} ,"app.open.money") OR
      contains(${version_migrations.migrated_to} ,"m.open.money")
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    y_axes: [{label: '', orientation: left, series: [{axisId: external_fund_transfers.count,
            id: external_fund_transfers.count, name: External Fund Transfers}], showLabels: true,
        showValues: true, unpinAxis: false, tickDensity: default, type: linear}, {
        label: '', orientation: left, series: [{axisId: sum_in_crore, id: sum_in_crore,
            name: Sum in crore}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, type: linear}]
    hidden_series: []
    series_types:
      sum_in_crore: column
    series_colors:
      users.count: "#12B5CB"
      external_fund_transfers.sum: "#079c98"
      external_fund_transfers.count: "#C4261D"
      sum_in_billion_rupee: "#079c98"
      sum_in_billion: "#079c98"
      sum_in_crore: "#00518F"
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_fields: [external_fund_transfers.sum]
    listen: {}
    row: 20
    col: 12
    width: 12
    height: 5
