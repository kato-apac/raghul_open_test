- dashboard: open_money_partner_bank_connected_accounts
  title: open.money partner bank connected accounts
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  elements:
  - title: 'ICICI connected accounts DOD '
    name: 'ICICI connected accounts DOD '
    model: raghul_test_model
    explore: icici_connected_accounts
    type: looker_area
    fields: [icici_connected_accounts.count, icici_connected_accounts.created_date]
    fill_fields: [icici_connected_accounts.created_date]
    filters:
      icici_connected_accounts.created_date: 15 days
      users.source_of_registration: m.open.money,app.open.money
      icici_connected_accounts.bank_connection_statuses_id: '1,2'
    sorts: [icici_connected_accounts.created_date desc]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      icici_connected_accounts.count: "#F99D27"
    ordering: none
    show_null_labels: false
    defaults_version: 1
    listen: {}
    row: 0
    col: 12
    width: 12
    height: 6
  - title: Yes Bank Connected accounts DOD
    name: Yes Bank Connected accounts DOD
    model: openfinancialtech
    explore: yes_bank_connected_accounts
    type: looker_area
    fields: [yes_bank_connected_accounts.count, yes_bank_connected_accounts.created_date]
    fill_fields: [yes_bank_connected_accounts.created_date]
    filters:
      yes_bank_connected_accounts.created_date: 15 days
      users.source_of_registration: m.open.money,app.open.money
      yes_bank_connected_accounts.bank_connection_statuses_id: '1,2'
    sorts: [yes_bank_connected_accounts.created_date desc]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      yes_bank_connected_accounts.count: "#C4261D"
    ordering: none
    show_null_labels: false
    defaults_version: 1
    listen: {}
    row: 6
    col: 12
    width: 12
    height: 6
  - title: Yes bank connected accounts MOM
    name: Yes bank connected accounts MOM
    model: openfinancialtech
    explore: yes_bank_connected_accounts
    type: looker_column
    fields: [yes_bank_connected_accounts.count, yes_bank_connected_accounts.created_month]
    fill_fields: [yes_bank_connected_accounts.created_month]
    filters:
      users.source_of_registration: m.open.money,app.open.money
      yes_bank_connected_accounts.bank_connection_statuses_id: '1,2'
      yes_bank_connected_accounts.created_month: 6 months
    sorts: [yes_bank_connected_accounts.created_month desc]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      yes_bank_connected_accounts.count: "#00518F"
    show_null_points: true
    interpolation: linear
    defaults_version: 1
    row: 6
    col: 0
    width: 12
    height: 6
  - title: ICICI bank connected accounts MOM
    name: ICICI bank connected accounts MOM
    model: openfinancialtech
    explore: icici_connected_accounts
    type: looker_column
    fields: [icici_connected_accounts.count, icici_connected_accounts.created_month]
    fill_fields: [icici_connected_accounts.created_month]
    filters:
      users.source_of_registration: m.open.money,app.open.money
      icici_connected_accounts.bank_connection_statuses_id: '1,2'
      icici_connected_accounts.created_month: 6 months
    sorts: [icici_connected_accounts.created_month desc]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    series_colors:
      icici_connected_accounts.count: "#B02A30"
    show_null_points: true
    interpolation: monotone
    defaults_version: 1
    row: 0
    col: 0
    width: 12
    height: 6
