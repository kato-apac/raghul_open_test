connection: "open_redshift"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
include: "raghul_dashboard.dashboard.lookml"
# include: "/**/*.view.lkml"                 # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
datagroup: raghul_test_model_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

explore: card_transactions {
  join: card_details {
    type: left_outer
    sql_on: ${card_transactions.users_id} = ${card_details.card_details_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${card_transactions.users_id} = ${users.users_id}  ;;
    relationship: many_to_one
  }
}

explore: invoices {
  join: users {
    type: left_outer
    sql_on: ${invoices.users_id} = ${users.users_id} ;;
    relationship: many_to_one
  }
}

explore: gst_payments {
  join: users {
    type: left_outer
    sql_on: ${gst_payments.users_id} = ${users.users_id} ;;
    relationship: many_to_one
  }
}

explore: external_fund_transfers {}
